#include "cLastdreirad.h"


cLastdreirad::cLastdreirad()
    : cFahrrad(3, 3.8), cNutzrad(380.0), m_nutzlast(72.50)
{
}

cLastdreirad::~cLastdreirad()
    = default;

double cLastdreirad::zuladen(double lastplus)
{
    lastplus = (lastplus < 0) ? 0 : lastplus;
    return m_nutzlast += lastplus;
}

double cLastdreirad::abladen(double lastminus)
{
    lastminus = (lastminus < 0) ? 0 : lastminus;
    return m_nutzlast -= lastminus;
}

