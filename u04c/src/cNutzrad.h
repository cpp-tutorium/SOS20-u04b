#pragma once
#include "cFahrrad.h"


class cNutzrad : public virtual cFahrrad
{
private:
    double m_ertrag;
public:
    explicit cNutzrad(double ertrag);
    virtual ~cNutzrad();
    
    double einkommen(double einkommen);
    double wartungmachen(double kosten);    
};
