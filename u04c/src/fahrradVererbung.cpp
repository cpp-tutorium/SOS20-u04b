/*
 * u04b
 * Name:    Linus Bleyl
 * Datum:   11.04.2019
 */

#include "cEinrad.h"
#include "cRikscha.h"
#include "cLastdreirad.h"
#include "cMountainbike.h"
#include <iostream>
#include "cRennrad.h"

int main(int argc, char* argv[])
{
    cLastdreirad hauruck;
    cRikscha einergehtnochrein;
    cMountainbike downhillstar;
    cEinrad obenbleiben;
    cRennrad nodope;

    std::cout << "Radzahl des Lastdreirads: " << hauruck.get_radzahl() << std::endl; 
    std::cout << "Radzahl der Rikscha: " << einergehtnochrein.get_radzahl() << std::endl; 
    std::cout << "Radzahl des Mountainbikes: " << downhillstar.get_radzahl() << std::endl; 
    std::cout << "Radzahl des Einrads: " << obenbleiben.get_radzahl() << std::endl;

    std::cout << "Mountainbike nach 300 meter downhill, spass = " << downhillstar.downhill(-300) << std::endl; 
    std::cout << "Mountainbike bekommt einen Steinschlag" << std::endl; 
    
    downhillstar.steinschlag();
    std::cout << "Mountainbike nach Steinschlag, spass = " << downhillstar.downhill(0) << std::endl; 
    std::cout << "Mountainbike bekommt einen Steinschlag" << std::endl; 
    
    downhillstar.steinschlag(); 
    std::cout << "Mountainbike nach Steinschlag, spass = " << downhillstar.downhill(0) << std::endl;

    std::cout << "Radzahl des Rennrads: " << nodope.get_radzahl() << std::endl; 
    nodope.aufpumpen(1.2);
    std::cout << "Rennrad nach Aufpumpen, luftdruck = " << nodope.get_luftdruck() << std::endl;

    std::cout << "Ende" << std::endl;

    return 0;
}
