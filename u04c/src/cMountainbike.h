#pragma once
#include "cFreizeitrad.h"


class cMountainbike : public cFreizeitrad
{
public:
    explicit cMountainbike();
    ~cMountainbike();

    double downhill(int hoehendifferenz);
    void steinschlag();

private:
    static void schimpfen();
};

