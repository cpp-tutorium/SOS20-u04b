#include "cRennrad.h"



//cRennrad::cRennrad()
//    : cFahrrad(2, 5.4), cNutzrad(0.0), cFreizeitrad(75.0), m_gang(1)
//{
//}


cRennrad::cRennrad(double ertrag_in, double spass_in, int gang_in, int rad_in, double druck_in)
	: cFahrrad(rad_in, druck_in), cNutzrad(ertrag_in), cFreizeitrad(spass_in), m_gang(1)
{
}

cRennrad::~cRennrad()
    = default;

double cRennrad::aufpumpen(double druckplus)
{
    druckplus += m_luftdruck;
    m_luftdruck = (druckplus > 6) ? 6 : druckplus;
    return m_luftdruck;
}
