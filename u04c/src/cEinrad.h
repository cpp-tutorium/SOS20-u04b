#pragma once
#include "cFreizeitrad.h"

class cEinrad : public cFreizeitrad
{
public:
    explicit cEinrad();
    ~cEinrad();

    double geniessen(double genuss) override;
};
