#pragma once

class cFahrrad
{
protected:
    int m_radzahl;
    double m_luftdruck;
public:
    explicit cFahrrad(int radzahl, double luftdruck);
    virtual ~cFahrrad();

    int get_radzahl() const;
    double get_luftdruck() const;
    virtual double aufpumpen(double druckplus);
};

