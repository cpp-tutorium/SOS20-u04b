#pragma once
#include "cNutzrad.h"

class cRikscha : public cNutzrad
{
private:
    int m_fahrgastzahl;
public:
    explicit cRikscha();
    ~cRikscha();

    int einsteigen(int rein);
    int aussteigen(int raus);
};

