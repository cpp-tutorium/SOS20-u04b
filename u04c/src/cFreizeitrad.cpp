#include "cFreizeitrad.h"

cFreizeitrad::cFreizeitrad(const double spass)
    : cFahrrad(0, 0.0), m_spass(spass)
{
}

cFreizeitrad::~cFreizeitrad()
    = default;

void cFreizeitrad::abschlissen(const bool disziplin)
{
    m_spass += (disziplin) ? 20 : 0.0;
}

double cFreizeitrad::geniessen(const double genuss)
{
    return m_spass += genuss;
}


