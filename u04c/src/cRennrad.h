#pragma once
#include "cNutzrad.h"
#include "cFreizeitrad.h"


class cRennrad : public cNutzrad, public cFreizeitrad
{
private:
    int m_gang;
public:
    cRennrad(double ertrag_in = 0.0, double spass_in = 75.00, int gang_in = 18, int rad_in = 2, double druck_in = 5.4);
    ~cRennrad();

    double aufpumpen(double druckplus) override;
};

