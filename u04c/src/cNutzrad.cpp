#include "cNutzrad.h"

cNutzrad::cNutzrad(const double ertrag)
    : cFahrrad(0, 0.0), m_ertrag(ertrag)
{
}

cNutzrad::~cNutzrad()
    = default;

double cNutzrad::einkommen(const double einkommen)
{
    m_ertrag += einkommen;
    return m_ertrag;
}

double cNutzrad::wartungmachen(const double kosten)
{
    m_ertrag -= kosten;
    return m_ertrag;
}
