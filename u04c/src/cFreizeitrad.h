#pragma once
#include "cFahrrad.h"


class cFreizeitrad : public virtual cFahrrad
{
protected:
    double m_spass;
public:
    explicit cFreizeitrad(double spass);
    virtual ~cFreizeitrad();

    void abschlissen(bool disziplin);
    virtual double geniessen(double genuss);
};
