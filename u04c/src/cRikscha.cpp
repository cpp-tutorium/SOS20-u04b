#include "cRikscha.h"


cRikscha::cRikscha()
    : cFahrrad(4, 2.7), cNutzrad(620.0), m_fahrgastzahl(1)
{
}

cRikscha::~cRikscha()
    = default;

int cRikscha::einsteigen(int rein)
{
    rein = (rein < 0) ? 0 : rein;
    return m_fahrgastzahl += rein;
}

int cRikscha::aussteigen(int raus)
{
    raus = (raus < 0) ? 0 : raus;
    m_fahrgastzahl -= raus;
    return m_fahrgastzahl = (m_fahrgastzahl < 0) ? 0 : m_fahrgastzahl; 
}

