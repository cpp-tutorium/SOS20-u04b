#include "cFahrrad.h"


cFahrrad::cFahrrad(const int radzahl, const double luftdruck)
    : m_radzahl(radzahl), m_luftdruck(luftdruck)
{
}

cFahrrad::~cFahrrad()
    = default;

int cFahrrad::get_radzahl() const
{
    return m_radzahl;
}

double cFahrrad::get_luftdruck() const
{
    return m_luftdruck;
}

double cFahrrad::aufpumpen(const double druckplus)
{
    m_luftdruck += druckplus;
    return m_luftdruck;
}


