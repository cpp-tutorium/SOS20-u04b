#include "cMountainbike.h"
#include <iostream>

cMountainbike::cMountainbike()
    : cFahrrad(2, 1.3), cFreizeitrad(100.0)
{
}

cMountainbike::~cMountainbike()
    = default;

double cMountainbike::downhill(const int hoehendifferenz)
{
    m_spass += (10 * hoehendifferenz);
    m_spass = (m_spass < 0) ? 0 : m_spass;
    return m_spass;
}

void cMountainbike::steinschlag()
{
    m_spass -= 2000;

    if (m_spass > 0)
        return;

    m_spass = 0;
    schimpfen();    
}

void cMountainbike::schimpfen()
{
    std::cout << "Schon wieder ein Stein, macht keinen Spass mehr" << std::endl;
}


