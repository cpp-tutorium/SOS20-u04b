#pragma once
#include "cNutzrad.h"

class cLastdreirad : public cNutzrad
{
private:
    double m_nutzlast;
public:
    explicit cLastdreirad();
    ~cLastdreirad();

    double zuladen(double lastplus);
    double abladen(double lastminus);
};
