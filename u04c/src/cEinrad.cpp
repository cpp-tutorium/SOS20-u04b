#include "cEinrad.h"

cEinrad::cEinrad()
    : cFahrrad(1, 2.3) , cFreizeitrad(200.0)
{
}

cEinrad::~cEinrad()
    = default;

double cEinrad::geniessen(const double genuss)
{
    return m_spass += (genuss < 0) ? 20 * genuss : 5 * genuss;
}

